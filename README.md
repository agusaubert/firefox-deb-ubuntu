# firefox-deb-ubuntu

This procedure applies if you want to replace firefox snap with a firefox deb package installation in Ubuntu 22.x

It complements the original guide [How to Install Firefox as DEB on Ubuntu (Not Snap)](https://www.omgubuntu.co.uk/2022/04/how-to-install-firefox-deb-apt-ubuntu-22-04) 

1) Remove snap
2) Download repo key
3) Import repo key
4) Set priority
5) Create and parse apparmor configuration



## Remove Firefox snap
```
sudo snap remove firefox
```

## Download Mozilla repo key 
```
sudo curl -sq -o /etc/apt/keyrings/packages.mozilla.org.asc https://packages.mozilla.org/apt/repo-signing-key.gpg
```
## Import Mozilla repo key
```
sudo echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" >> /etc/apt/sources.list.d/mozilla.list
```

## Set preference to install deb over snap
```
echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | sudo tee /etc/apt/preferences.d/mozilla
```

## If apparmor is enabled, create configuration file and parse it
```
sudo curl -qs -o /etc/apparmor.d/usr.bin.firefox https://gitlab.com/agusaubert/firefox-deb-ubuntu/-/raw/main/usr.bin.firefox
```

```
sudo apparmor_parser -r /etc/apparmor.d/usr.bin.firefox
```
 